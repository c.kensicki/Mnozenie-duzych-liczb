#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv) {
	int errs = 0;
    int provided, procno,procs;
    int l1, l2, warunek; // l1 - dlugosc pierwszej liczby, l2 - dlugosc drugiej liczby, warunek - zmienna nadajaca dlugosc tablicy sumujacej
    warunek = 100;
    l1 = 7;
    l2 = 12;
    MPI_Init_thread( 0, 0, MPI_THREAD_MULTIPLE, &provided );
    MPI_Comm_rank(MPI_COMM_WORLD,&procno);
    MPI_Comm_size(MPI_COMM_WORLD,&procs);
    int tab1[ l1 ];
    int tab2[ l2 ];
    if( l1 > l2 )
    {
        warunek = l1;
    }
    else
    {
        warunek = l2;
    }
    int tab3[ warunek ];
    int i, j, reszta, wynik;
    for( i = 0; i < warunek; i++ )
    {
        tab1[ i ] = 0;
        tab2[ i ] = 0;
        tab3[ i ] = 0;
    }
    tab3[ warunek ] = 0;
    printf( "Podaj pierwsza liczbe: \n" );
    for( i =( l1 - 1 ); i >= 0; i-- )
         scanf( "%1i", & tab1[ i ] );
    
    printf( "Pierwsza liczba to: \n" );
    for( j = l1 - 1; j >= 0; j-- )
         printf( "%i", tab1[ j ] );
    
    printf( "\nPodaj druga liczbe: \n" );
    for( i =( l2 - 1 ); i >= 0; i-- )
         scanf( "%1i", & tab2[ i ] );
    
    printf( "Druga liczba to: \n" );
    for( j =( l2 - 1 ); j >= 0; j-- )
         printf( "%i", tab2[ j ] );
    
    printf( "\nMnozenie:\n" );
    reszta = 0; // reszta z mnozenia dwoch tablic
    for( i = 0; i <= warunek; i++ )
    {
        if( i < warunek )
        {
            wynik = tab1[ i ] * tab2[ i ] + reszta;
            if( wynik > 9 && wynik <100){
                wynik -= 10;
                reszta = wynik;
            }
            else if (wynik > 99 && wynik <1000)
            {
				wynik -= 100;
                reszta = wynik;
			}
            else
                 reszta = 0;
            
            tab3[ i ] = wynik;
        }
        else
             tab3[ warunek ] = reszta;
        
    }
    if( tab3[ warunek ] != 0 ) // decyzja o wypisaniu pierwszego elementu tablicy
         printf( "%d", tab3[ warunek ] );
    
    for( i = warunek - 1; i >= 0; i-- )
    {
        
        printf( "%d", tab3[ i ] );
    }

    MPI_Finalize();
    return errs;
}
